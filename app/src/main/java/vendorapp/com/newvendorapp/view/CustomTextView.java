package vendorapp.com.newvendorapp.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by netset on 4/30/2018.
 */

public class CustomTextView  extends android.support.v7.widget.AppCompatTextView{
    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"font/HelveticaNeueLTStd-Md_0.otf");
        setTypeface(tf, 1);
    }

}
