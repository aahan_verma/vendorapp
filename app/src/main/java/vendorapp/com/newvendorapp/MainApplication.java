package vendorapp.com.newvendorapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class MainApplication extends Application {
    public static  MainApplication mainApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        mainApplication = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static synchronized  MainApplication getInstance(){
        return mainApplication;
    }
}
