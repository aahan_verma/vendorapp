package vendorapp.com.newvendorapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;

import java.util.ArrayList;
import java.util.List;

import vendorapp.com.newvendorapp.R;
import vendorapp.com.newvendorapp.adapter.NavigationViewAdapter;
import vendorapp.com.newvendorapp.common.CommonHelper;
import vendorapp.com.newvendorapp.fragments.HomeFragment;
import vendorapp.com.newvendorapp.interf.onRecyclerViewClick;
import vendorapp.com.newvendorapp.models.NavigationViewList;

public class HomeActivity extends AppCompatActivity implements onRecyclerViewClick {
    DrawerLayout drawer;
    ImageView nav_ham;
    RecyclerView recyclerView;
    List<NavigationViewList> navigationViewList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }
        if (savedInstanceState == null) {
            Fragment fragment = new HomeFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        }
        init();
        onClick();
    }

    private void onClick() {
        nav_ham.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    private void init() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_ham = findViewById(R.id.nav_ham);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        NavigationViewAdapter navigationViewAdapter = new NavigationViewAdapter(navigationViewList, this, HomeActivity.this);
        recyclerView.setAdapter(navigationViewAdapter);

        NavigationViewList navView = new NavigationViewList("Home");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Driver Management");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Order Management");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Product Management");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Risk Management");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Setting");
        navigationViewList.add(navView);
        navView = new NavigationViewList("Logout");
        navigationViewList.add(navView);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onRecyClick(int pos) {
        if (pos == 6) {
            drawer.closeDrawers();
            AlertView alert  = new AlertView("Logout","Are you sure you want to logout", AlertStyle.IOS);
            alert.addAction(new AlertAction("Yes", AlertActionStyle.DEFAULT, action -> {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                finishAffinity();
            }));


            alert.show(this);
        }

    }
}
