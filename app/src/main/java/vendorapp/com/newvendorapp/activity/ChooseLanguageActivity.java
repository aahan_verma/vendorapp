package vendorapp.com.newvendorapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import vendorapp.com.newvendorapp.R;
import vendorapp.com.newvendorapp.common.CommonHelper;

public class ChooseLanguageActivity extends AppCompatActivity {

    Button btn_next, btn_arabic, btn_english;
    RelativeLayout toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.langugage_activity);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        init();
        onCLick();
    }

    private void onCLick() {
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        btn_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_arabic.setBackground(getResources().getDrawable(R.drawable.custom_curver_unselected_bg));
                btn_arabic.setTextColor(getResources().getColor(R.color.white));
                btn_english.setBackground(getResources().getDrawable(R.drawable.custom_curved_bg));
                btn_english.setTextColor(getResources().getColor(R.color.black));
            }
        });
        btn_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_english.setBackground(getResources().getDrawable(R.drawable.custom_curver_unselected_bg));
                btn_english.setTextColor(getResources().getColor(R.color.white));
                btn_arabic.setBackground(getResources().getDrawable(R.drawable.custom_curved_bg));
                btn_arabic.setTextColor(getResources().getColor(R.color.black));
            }
        });
    }

    private void init() {
        btn_next = findViewById(R.id.next_btn);
        btn_arabic = findViewById(R.id.arabic_btn);
        btn_english = findViewById(R.id.english_btn);
        toolbar = findViewById(R.id.toolbar);

    }
}
