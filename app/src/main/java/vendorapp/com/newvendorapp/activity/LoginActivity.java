package vendorapp.com.newvendorapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import vendorapp.com.newvendorapp.R;
import vendorapp.com.newvendorapp.common.CommonHelper;

public class LoginActivity extends AppCompatActivity {

    EditText et_email, et_password;
    TextView tv_frgtpwd;
    Button btn_login;
    ImageView iv_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        init();
        onClick();
    }

    private void onClick() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    dostartActivity(HomeActivity.class);
                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_frgtpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActivity(ForgotPassword.class);
            }
        });
    }

    private void dostartActivity(Class<?> activity) {
        Intent intent = new Intent(this,activity);
        startActivity(intent);
        overridePendingTransition(R.anim.enter,R.anim.exit);
    }

    private boolean validate() {
        String email = et_email.getText().toString().trim();
        String password = et_password.getText().toString().trim();

        if (email.isEmpty()) {
            CommonHelper.ErrorDialog(this, "Please enter registered email address.");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            CommonHelper.ErrorDialog(this, "Please enter valid email address.");
            return false;
        } else if (password.isEmpty()) {
            CommonHelper.ErrorDialog(this, "Please enter password.");
            return false;
        }
        return true;
    }

    private void init() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_frgtpwd = findViewById(R.id.forgotPwdTv);
        btn_login = findViewById(R.id.btn_login);
        iv_back = findViewById(R.id.back_arrow);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }
}
