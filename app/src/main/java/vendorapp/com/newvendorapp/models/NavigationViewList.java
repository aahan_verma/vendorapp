package vendorapp.com.newvendorapp.models;

public class NavigationViewList {
    String mTitle;

    public NavigationViewList(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}
